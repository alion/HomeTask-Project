public class Car {
    private String model;
    private String engine;
    private String speed;
    Client buyer;


    private String type;
    private String year;

    public String getModel() {
        return model;
    }

    public String getEngine() {
        return engine;
    }

    public String getSpeed() {
        return speed;
    }

    public String getType() {
        return type;
    }

    public String getYear() {
        return year;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Car(String model, String engine, String speed, String type, String year) {
        this.model = model;
        this.engine = engine;
        this.speed = speed;
        this.type = type;
        this.year = year;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", engine='" + engine + '\'' +
                ", speed='" + speed + '\'' +
                ", type='" + type + '\'' +
                ", year='" + year + '\'' +
                '}';
    }

}

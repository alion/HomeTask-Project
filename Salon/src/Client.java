public class Client {

    private String name;
    private String surname;
    private String buyDay;


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getBuyDay() {
        return buyDay;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBuyDay(String buyDay) {
        this.buyDay = buyDay;
    }

    public Client(String name, String surname, String buyDay) {
        this.name = name;
        this.surname = surname;
        this.buyDay = buyDay;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", buyDay='" + buyDay + '\'' +
                '}';
    }
}

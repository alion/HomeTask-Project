class Family {

    private String name;
    private String gender;
    private int age;
    private String FamilyRole;

    Family(String name, String gender, int age, String FamilyRole) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.FamilyRole = FamilyRole;
    }

    public   void Print() {
        System.out.println(" Имя: " + name + " Пол: " + gender + " Возраст: " + age + " Роль в семье: " + FamilyRole);

    }
}
import java.util.List;

public class Application {
    public static void main(String[] args) {
        Salon salon = new Salon();
        salon.clients[0] = new Client("Yury", "Yury", "august");
        salon.clients[1] = new Client("Stas", "Stas", "may");
        salon.clients[2] = new Client("Stas1", "Stas1", "may1");
        salon.clients[1] = new Client("Stas2", "Stas2", "may2");
        salon.cars[0] = new Car("BMW", "2", "100", "хэтчбэк", "2018");
        salon.cars[1] = new Car("Volvo", "2", "200", "cross", "2019");
        salon.cars[2] = new Car("Audi", "2", "200", "cross", "2009");
        salon.cars[3] = new Car("Lada", "2", "200", "cross", "2005");
        salon.cars[1].buyer = salon.clients[2];

        salon.carsPrint();
        salon.clientsPrint();
        salon.soldPrint();
    }
}
